//
//  SecondViewController.swift
//  FinalPokeDex
//
//  Created by Developer on 25/06/19.
//  Copyright © 2019 Gonçalo Pereira. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tab: UITableView!
    var index:Int = 5
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PokemonsArray.pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = PokemonsArray.pokemons[indexPath.row].Nome
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        performSegue(withIdentifier: "PokemonDetail", sender: indexPath.row)

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let dest = segue.destination as? PokemonView  else {
            
            let alert = UIAlertController(title: "Erro", message: "Destino Errado", preferredStyle: .alert)
            
            let btn = UIAlertAction(title: "ok", style: .cancel, handler: { a in
                self.teste(a:"")
            })
            
            alert.addAction(btn)
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        dest.Nome = sender as? String ?? "empty"
        dest.Descricao = sender as? String ?? "empty"
        dest.Forca = sender as? String ?? "empty"
        dest.Hp = sender as? String ?? "empty"
        dest.Lista_de_ataques = sender as? String ?? "empty"
        dest.Possiveis_evolucoes = sender as? String ?? "empty"
        dest.Sub_tipo = sender as? String ?? "empty"
        dest.Tipo = sender as? String ?? "empty"
        dest.Imagem = sender as? String ?? "empty"
        dest.index = index

    }
    
    func teste(a:String)  {
        print("Ola Mundo \(a)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("laod")
        
        PokemonsArray.pokemons.append(Pokemon(Nome: "Pokemon 1", Xp: "12", Hp: "11", Descricao: "sfdsfs", Lista_de_ataques: "sss", Tipo: "tt", Sub_tipo: "ds", Forca: "2", Possiveis_evolucoes: "ds", Imagem: "www.google.com"))
    }


}

