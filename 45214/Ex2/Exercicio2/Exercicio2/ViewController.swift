//
//  ViewController.swift
//  Exercicio2
//
//  Created by Developer on 14/05/19.
//  Copyright © 2019 goncaloistec. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
var lbl:UILabel?
    var lblR:UILabel?
    var lblG:UILabel?
    var lblB:UILabel?
    var lblA:UILabel?
    var slider1:UISlider?
    var slider2:UISlider?
    var slider3:UISlider?
    var slider4:UISlider?

    var r:Float?=0
    var g:Float?=0
    var b:Float?=0
    var a:Float?=0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl=UILabel(frame: CGRect(x:115,y:100, width:180, height:150))
        
        self.lbl?.backgroundColor = UIColor.gray
        self.lbl?.textAlignment = .center
        
        self.lblR=UILabel(frame: CGRect(x:0,y:300, width:60, height:20))
        
         self.lblR?.text="R: 0"
         self.lblR?.textAlignment = .center
        
         self.lblG=UILabel(frame: CGRect(x:50,y:300, width:60, height:20))
        
         self.lblG?.text="G: 0"
         self.lblG?.textAlignment = .center
        
         self.lblB=UILabel(frame: CGRect(x:100,y:300, width:60, height:20))
        
         self.lblB?.text="B: 0"
         self.lblB?.textAlignment = .center
        
         self.lblA=UILabel(frame: CGRect(x:150,y:300, width:60, height:20))
        
         self.lblA?.text="A: 0"
         self.lblA?.textAlignment = .center
        
         self.slider1=UISlider(frame: CGRect(x:5,y:350,width:400,height:70))
        
         self.slider2=UISlider(frame: CGRect(x:5,y:400,width:400,height:70))
        
         self.slider3=UISlider(frame: CGRect(x:5,y:450,width:400,height:70))
        
         self.slider4=UISlider(frame: CGRect(x:5,y:500,width:400,height:70))
        
         self.view.addSubview(lbl!)
         self.view.addSubview(lblR!)
         self.view.addSubview(lblG!)
         self.view.addSubview(lblB!)
         self.view.addSubview(lblA!)
         self.view.addSubview(slider1!)
         self.view.addSubview(slider2!)
         self.view.addSubview(slider3!)
         self.view.addSubview(slider4!)
          slider1?.maximumValue=255
          slider2?.maximumValue=255
          slider3?.maximumValue=255
          slider4?.maximumValue=1
        
          slider1?.addTarget(self, action: #selector(self.changeValue), for: .valueChanged)
          slider2?.addTarget(self, action: #selector(self.changeValue), for: .valueChanged)
          slider3?.addTarget(self, action: #selector(self.changeValue), for: .valueChanged)
          slider4?.addTarget(self, action: #selector(self.changeValue), for: .valueChanged)

        
    }
    
  
@objc func changeValue() {
    
        r=(slider1!.value)
        g=(slider2!.value)
        b=(slider3!.value)
        a=(slider4!.value)
        
        lblR?.text="R: \(Int(r!))"
        lblG?.text="G: \(Int(g!))"
        lblB?.text="B: \(Int(b!))"
        lblA?.text="A: \(Int(a!))"
    let swiftColor = UIColor(red: CGFloat(r!)/255, green: CGFloat(g!)/255, blue: CGFloat(b!)/255, alpha: CGFloat(a!)/1)
    self.lbl?.backgroundColor=swiftColor
 }


  }
