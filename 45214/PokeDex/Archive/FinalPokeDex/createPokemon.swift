//
//  createPokemon.swift
//  FinalPokeDex
//
//  Created by Developer on 02/07/19.
//  Copyright © 2019 Gonçalo Pereira. All rights reserved.
//

import UIKit

class createPokemon: UIViewController {
   
    @IBOutlet weak var Nome: UITextField!
    
    @IBOutlet weak var xp: UITextField!
    
    @IBOutlet weak var hp: UITextField!
    
    @IBOutlet weak var descr: UITextView!
    
    @IBOutlet weak var listadeataques: UITextView!
    
    @IBOutlet weak var tipo: UITextField!
    
    @IBOutlet weak var subtipo: UITextField!
    
    @IBOutlet weak var forca: UITextField!
    
    @IBOutlet weak var possiveisevolucoes: UITextView!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       // var vc = segue.destination as! SecondViewController
        /*vc.finalName = self.nameText*/
    }

    
    
    @IBAction func createPokemon(_ sender: UIButton) {
     print(PokemonsArray.pokemons)
        PokemonsArray.pokemons.append(Pokemon(Nome: Nome.text!, Xp: xp.text!, Hp: hp.text!, Descricao: descr.text!, Lista_de_ataques: listadeataques.text!, Tipo: tipo.text!, Sub_tipo: subtipo.text!, Forca: forca.text! , Possiveis_evolucoes: possiveisevolucoes.text!, Imagem: ""))
        
    Nome.text = ""
    xp.text = ""
    hp.text = ""
    descr.text = ""
    listadeataques.text = ""
    tipo.text = ""
    subtipo.text = ""
    forca.text = ""
    possiveisevolucoes.text = ""
    (self.navigationController?.viewControllers[2] as? SecondViewController)?.tab.reloadData()
    let alert = UIAlertController(title: "Saved", message: "Foi guardado com sucesso!", preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
    self.present(alert, animated: true, completion: nil)
   
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
    }
    
    
}
