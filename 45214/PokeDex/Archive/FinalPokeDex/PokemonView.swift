import UIKit

class PokemonView: UIViewController {
    
     var Nome:String?
     var Xp:String?
     var Hp:String?
     var Descricao:String?
     var Lista_de_ataques:String?
     var Tipo:String?
     var Sub_tipo:String?
     var Forca:String?
     var Possiveis_evolucoes :String?
     var Imagem:String?
     var index:Int = 5
    
   
    @IBOutlet weak var RemoveButton: UIButton!
    
    @IBOutlet weak var bttSave: UIButton!
    
    @IBOutlet weak var updateButton: UIButton!
    
    
    @IBAction func Save(_ sender: UIButton) {
        (PokemonsArray.pokemons[index].Nome) = txtnome.text ?? ""
        (PokemonsArray.pokemons[index].Xp) = txtxp.text ?? ""
        (PokemonsArray.pokemons[index].Hp) = txthp.text ?? ""
        (PokemonsArray.pokemons[index].Lista_de_ataques) = txtlistadeataques.text ?? ""
        (PokemonsArray.pokemons[index].Tipo) =  txttipo.text ?? ""
        (PokemonsArray.pokemons[index].Sub_tipo) = txtsubtipo.text ?? ""
        (PokemonsArray.pokemons[index].Forca) = txtforca.text ?? ""
        (PokemonsArray.pokemons[index].Possiveis_evolucoes) = txtevolucoes.text ?? ""
        (PokemonsArray.pokemons[index].Descricao) = txtdescr.text ?? ""
        
        let alert = UIAlertController(title: "Saved", message: "Foi Editado com sucesso!", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        txtnome.isEnabled = false
        txtxp.isEnabled = false
        txthp.isEnabled = false
        txtlistadeataques.isEnabled = false
        txttipo.isEnabled = false
        txtsubtipo.isEnabled = false
        txtforca.isEnabled = false
        txtevolucoes.isEnabled = false
        txtdescr.isSelectable = false
        bttSave.isHidden = true
    }
    
    @IBAction func Edit(_ sender: UIButton) {
        txtnome.isEnabled = true
        txtxp.isEnabled = true
        txthp.isEnabled = true
        txtlistadeataques.isEnabled = true
        txttipo.isEnabled = true
        txtsubtipo.isEnabled = true
        txtforca.isEnabled = true
        txtevolucoes.isEnabled = true
        txtdescr.isSelectable = true
        bttSave.isHidden = false
    }
    
    @IBOutlet weak var txtnome: UITextField!
    
    @IBOutlet weak var txtxp: UITextField!
    
    @IBOutlet weak var txthp: UITextField!
    
    @IBOutlet weak var txtlistadeataques: UITextField!
    
    @IBOutlet weak var txttipo: UITextField!
    
    @IBOutlet weak var txtsubtipo: UITextField!
    
    @IBOutlet weak var txtforca: UITextField!
    
    @IBOutlet weak var txtevolucoes: UITextField!
    
    @IBOutlet weak var txtdescr: UITextView!
    
    @IBAction func Delete(_ sender: UIButton) {
        //print("delete olaaaaaa")
     PokemonsArray.pokemons.remove(at: index)
        let alert = UIAlertController(title: "Deleted", message: "Foi apagado com sucesso!", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        //self.dismiss(animated: true)
    
        (self.navigationController?.viewControllers[0] as? SecondViewController)?.tab.reloadData()
        RemoveButton.isEnabled = false
        updateButton.isEnabled = false
               
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let dest = segue.destination as? SecondViewController  else {
            
            let alert = UIAlertController(title: "Erro", message: "Destino Errado", preferredStyle: .alert)
            
            let btn = UIAlertAction(title: "ok", style: .cancel, handler: { a in
                self.teste(a:"")
            })
            
            alert.addAction(btn)
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        
        
    }
    
    func teste(a:String)  {
        print("Ola Mundo \(a)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     txtnome.text = (PokemonsArray.pokemons[index].Nome)
     txtxp.text = (PokemonsArray.pokemons[index].Xp)
     txthp.text = (PokemonsArray.pokemons[index].Hp)
     txtlistadeataques.text = (PokemonsArray.pokemons[index].Lista_de_ataques)
     txttipo.text = (PokemonsArray.pokemons[index].Tipo)
     txtsubtipo.text = (PokemonsArray.pokemons[index].Sub_tipo)
     txtforca.text = (PokemonsArray.pokemons[index].Forca)
     txtevolucoes.text = (PokemonsArray.pokemons[index].Possiveis_evolucoes)
     txtdescr.text = (PokemonsArray.pokemons[index].Descricao)
        
        txtnome.isEnabled = false
        txtxp.isEnabled = false
        txthp.isEnabled = false
        txtlistadeataques.isEnabled = false
        txttipo.isEnabled = false
        txtsubtipo.isEnabled = false
        txtforca.isEnabled = false
        txtevolucoes.isEnabled = false
        txtdescr.isSelectable = false
        
        bttSave.isHidden = true
    }
    
    
}
