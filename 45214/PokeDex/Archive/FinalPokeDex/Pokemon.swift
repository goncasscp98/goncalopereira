
import Foundation

class Pokemon {
    
    var Nome:String
    var Xp:String
    var Hp:String
    var Descricao:String
    var Lista_de_ataques:String
    var Tipo:String
    var Sub_tipo:String
    var Forca:String
    var Possiveis_evolucoes :String
    var Imagem:String
    
    init(Nome: String, Xp: String, Hp: String, Descricao: String, Lista_de_ataques: String, Tipo: String, Sub_tipo: String, Forca: String, Possiveis_evolucoes: String, Imagem: String) {
        self.Nome = Nome
        self.Xp = Xp
        self.Hp = Hp
        self.Descricao = Descricao
        self.Lista_de_ataques = Lista_de_ataques
        self.Tipo = Tipo
        self.Sub_tipo = Sub_tipo
        self.Forca = Forca
        self.Possiveis_evolucoes = Possiveis_evolucoes
        self.Imagem = "http://img.pokemondb.net/artwork/\(Imagem.components(separatedBy: " ")[0].lowercased()).jpg"
    }
}
